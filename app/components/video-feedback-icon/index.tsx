/* eslint-disable react-native/no-inline-styles */

import React, { FC } from "react"
import { Text, TouchableOpacity, View, Share } from "react-native"
import { styles } from "./video-feedback.style"
import Icon from 'react-native-vector-icons/FontAwesome'

export type VideoFeedbackIconProps = {
  content?:number,
  onPress():void,
  color:string,
  size:number,
  name:string,
  description?:string
}

  

const VideoFeedbackIcon: FC<VideoFeedbackIconProps> = (props) => {

  
  
  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity style={styles.iconContainer} onPress={props.onPress}>
          <Icon name={props.name} size={props.size} color={props.color} />
      </TouchableOpacity>
      <Text style={styles.textStyle}>{props.content === undefined ? props.description : props.content}</Text>
    </View>
  )
}

export default VideoFeedbackIcon

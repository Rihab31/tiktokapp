/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native"
import Colors from "../../utils/colors"

export const styles = StyleSheet.create({
 
  mainContainer: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center'
  },
  iconContainer:{
    marginTop:16,
    justifyContent:'center',
    alignItems:'center'
  },
  profileContainer:{

  },
  profilePictureWrapper:{

  },
  plusIconWrapper:{
    backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:10,
    height:20,
    width:20
  },
  textStyle:{
    color:Colors.white,
    fontSize:12
  },
 
})

/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
 
  mainContainer: {
    flex: 1,
  },
  iconContainer:{
    marginTop:16,
    justifyContent:'center',
    alignItems:'center'
  },
  
  plusIconWrapper:{
    backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:10,
    height:20,
    width:20,
    position:'absolute',
    bottom:-8,
    left:15
  },
  pictureContainer:{
    height:50,
    width:50,
    borderRadius:25,
    justifyContent:'center',
    alignItems:'center',
  },
  image:{
    height:40,
    width:40,
    borderRadius:20,
  }
 
})

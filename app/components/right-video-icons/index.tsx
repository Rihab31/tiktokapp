/* eslint-disable react-native/no-inline-styles */

import React, { FC, useState } from "react"
import { Image, Share, TouchableOpacity, View } from "react-native"
import { styles } from "./right-video-icons.style"
import Icon from 'react-native-vector-icons/FontAwesome'
import Colors from "../../utils/colors"
import VideoFeedbackIcon from "../video-feedback-icon"

export type VideoComponentProps = {
  comments:number
  likes:number
  userImage:string
  downloads:number
}

const onShare = async () => {
  try {
    const result = await Share.share({
      message:
        'React Native | A framework for building native apps using React',
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error:any) {
    console.log(error.message);
  }
};

const RightVideoIcons: FC<VideoComponentProps> = (props) => {
  
  const [likeClicked , setLikeClicked] = useState<boolean>(false)
  const [downloadClicked , setDownloadClicked] = useState<boolean>(false)
  
  return (
    <View style={styles.mainContainer}>
      <View >
        <TouchableOpacity style={styles.pictureContainer}>
        
        <Image source={{uri:props.userImage}} style={styles.image} />
    
        </TouchableOpacity>
        <TouchableOpacity style={styles.plusIconWrapper}>
        <Icon name="plus" size={15} color={Colors.white} />
        </TouchableOpacity>
      </View>

      <VideoFeedbackIcon content={props.likes} color={likeClicked ? Colors.red : Colors.white} size={30} name={"heart"} onPress={()=>{setLikeClicked(!likeClicked)}}/> 
      <VideoFeedbackIcon content={props.comments} color={Colors.white} size={30} name={"commenting"} onPress={()=>{}}/> 
      <VideoFeedbackIcon content={props.downloads} color={downloadClicked ? Colors.red : Colors.white} size={30} name={"bookmark"} onPress={()=>{setDownloadClicked(!downloadClicked)}}/> 
      <VideoFeedbackIcon description={"Partager"} color={Colors.white} size={30} name={"share"} onPress={()=>{onShare()}}/> 
       
    </View>
  )
}

export default RightVideoIcons

/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import Colors from "../../utils/colors"


const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
 
  mainContainer: {
    maxHeight:Dimensions.get('window').height - 60,
  },
  video:{
   height:Dimensions.get('window').height -60,
  },
  rightVideoIconsContainer:{
    position:"absolute",
    right:8,
    bottom:64
  },
  bottomContainer:{
    position:"absolute",
    height:"9%",
    flexDirection:'row',
    justifyContent:"space-between",
    alignItems:'center',
    bottom:0,
    width:"100%",
    padding:8
    
  },
  textContainer:{
    padding:16,
    justifyContent:'center',
  },
  pictureContainer:{
    height:50,
    width:50,
    borderRadius:25,
    justifyContent:'center',
    alignItems:'center',
  },
  textStyle:{
    color:Colors.white
  },
  image:{
    height:40,
    width:40,
    borderRadius:20,
  }
 
})

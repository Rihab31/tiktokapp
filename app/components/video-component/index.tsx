/* eslint-disable react-native/no-inline-styles */

import React, { FC, useRef, useState } from "react"
import { Animated, Easing, Image, Text, TouchableOpacity, View } from "react-native"
import { styles } from "./video-component.style"
import Video from 'react-native-video';
import RightVideoIcons from "../right-video-icons";

export type HomeCardProps = {
    onPress?(): void
    data: VideoComponentProps
  }
export type VideoComponentProps = {
    url:string
    userImage:string
    currentIndex:number
    itemIndex:number,
    comments:number,
    likes:number,
    userName:string,
    downloads:number
  }


const VideoComponent: FC<HomeCardProps> = (props) => {


    const spinValue = new Animated.Value(0);
    const player = useRef<any>();
    const [isPlaying,setIsPlaying] = useState<boolean>(false)


    Animated.loop(
      Animated.timing(
        spinValue,
        {
         toValue: 1,
         duration: 3000,
         easing: Easing.linear,
         useNativeDriver: true
        }
      )
     ).start();

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  })

  return (
    <View style={styles.mainContainer}>
     {
        props.data.currentIndex === props.data.itemIndex ?
        
       ( 
       <TouchableOpacity activeOpacity={1} style={styles.video} onPress={()=>{
         
         setIsPlaying(!isPlaying)}}>
            <Video source={{uri: props.data?.url}}
                    resizeMode={'contain'}
                    muted={false}
                    repeat  
                    style={styles.video}
                    ref={player}                                     
                    onBuffer={(buffer) => {
                        console.log("Buffering:",buffer)
                    }}               
                    onError={(error) => {
                        console.log("Error While Loading Video::",error);
                    }}
                    playInBackground={false}
                    playWhenInactive={false}
                    onLoadStart={() => {
                        console.log('onLoadStart', new Date());
                    }}
                    onLoad={() => {
                        console.log('onLoad', new Date());
                    }}
                    paused={isPlaying}
            />
       </TouchableOpacity>
      ) : <View style={styles.video}/>
     }
   <View style={styles.rightVideoIconsContainer}>
       <RightVideoIcons comments={props.data.comments} likes={props.data.likes} userImage={props.data.userImage} downloads={props.data.downloads} />
   </View>
   <View style={styles.bottomContainer}>
    <View style={styles.textContainer}>
        <Text style={styles.textStyle}>@{props.data.userName}</Text>
        <Text style={styles.textStyle}>Rihab</Text>
    </View>
   
    <View style={styles.pictureContainer}>
    <Animated.Image
          style={[styles.image,{transform: [{rotate: spin}] }]}
          source={{uri:props.data?.userImage}} />
       
    </View>
   </View>
    </View>
  )
}

export default VideoComponent

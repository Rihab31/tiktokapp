/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  imageWrapperStyle: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: screenWidth >= 768 ? 10 : 10,
    width: 70,
  },
  mainContainer: {
    flex: 1,
  },
  textStyle: {
    fontFamily: "Montserrat-Italic-VariableFont_wght",
    fontSize: 11,
  },
})

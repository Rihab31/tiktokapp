/* eslint-disable react-native/no-inline-styles */

import React, { FC } from "react"
import { Text, View, Image } from "react-native"
import { styles } from "./bottom-tab-icon.style"

export type BottomTabProps = {
  height:number
  icon: any
  label: string
  width:number
  color:string
}

const BottomTabIcon: FC<BottomTabProps> = (props) => {
  
  
  return (
    <View style={styles.mainContainer}>
      <View style={styles.imageWrapperStyle}>
       
          
        <Image source={props.icon} style={{height:props.height,width:props.width, marginTop:4,resizeMode:"contain"}}  />

        <Text style={[styles.textStyle, { color: props.color }]}>{props.label}</Text>
      </View>
    </View>
  )
}

export default BottomTabIcon

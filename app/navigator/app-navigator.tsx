/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/display-name */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-undef */
import React, { useState } from "react"
import {
  Image,
  useColorScheme,
  TouchableOpacity,
  View,
  Dimensions,
} from "react-native"
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  useNavigation,
} from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { navigationRef } from "./navigation-utilities"
import SplashScreen from "../screens/splash-screen"
import HomeScreen from "../screens/home-screen"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import BottomTabIcon from "../components/botton-tab-icon"
import  Colors  from "../utils/colors"
import ChatScreen from "../screens/chat-screen"
import ProfileScreen from "../screens/profile-screen"
import DiscoverScreen from "../screens/discover-screen"
import { CHAT, CHAT_OUTLINE, DISCOVER, DISCOVER_OUTLINE, HOME, HOME_OUTLINE, PLUS, PROFILE, PROFILE_OUTLINE } from "../utils/consts"


type Nav = {
  navigate: (value: string, params?: any) => void
}

export type NavigatorParamList = {
  splash: undefined
  home: undefined
}

export type HomeTabsParamList = {
  HOME: undefined
  Discover: undefined
  CREATE: undefined
  CHATS: undefined
  PROFILE: undefined
}

const Stack = createNativeStackNavigator<NavigatorParamList>()
const Tab = createBottomTabNavigator<HomeTabsParamList>()




const HomeTabNavigator = () => {
  const navigation = useNavigation<Nav>()
  
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        headerShown: false,
        tabBarBadgeStyle: {
          position: "absolute",
          top: 10,
        },
        tabBarStyle: {
          height: 60,
          backgroundColor: Colors.black,
        },
        headerTitleAlign: "center",
      }}
      initialRouteName="HOME"
    >
      <Tab.Screen
        name="HOME"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused }) => {
           
            return (
              <BottomTabIcon
                icon={focused ? HOME : HOME_OUTLINE}
                label={"Home"}
                height={20}
                width={20}
               color={Colors.white}
               
              />
            )
          },
        }}
      />
      <Tab.Screen
        name="Discover"
        component={DiscoverScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <BottomTabIcon
                icon={focused ? DISCOVER : DISCOVER_OUTLINE}
                label={"Découvrir"}
                height={20}
                width={20}
               color={Colors.white}
               
              />
            )
          },
        }}
      />

      <Tab.Screen
        name="CREATE"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TouchableOpacity
                style={{
                  bottom: 0,
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onPress={() => {
                  navigation.navigate("")
                }}
              >
                <Image
                  source={PLUS}
                  style={{ height: 30, width: 80,marginBottom:8, resizeMode:"contain" }}
                />
               
              </TouchableOpacity>
            )
          },
        }}
      />

      <Tab.Screen
        name="CHATS"
        component={ChatScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <BottomTabIcon
                icon={focused ? CHAT : CHAT_OUTLINE}
                label={"Chat"}
                height={20}
                width={20}
               color={Colors.white}
                
              />
            )
          },
        }}
      />
      <Tab.Screen
        name="PROFILE"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <BottomTabIcon
                icon={focused ? PROFILE : PROFILE_OUTLINE}
                label={"Profile"}
                height={20}
                width={20}
               color={Colors.white}
              />
            )
          },
        }}
      />
    </Tab.Navigator>
  )
}

const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="splash"
    >
      <Stack.Screen name="splash" component={SplashScreen} />
      <Stack.Screen name="home" component={HomeTabNavigator}/>
    </Stack.Navigator>
  )
}

interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = (props: NavigationProps) => {
  const colorScheme = useColorScheme()
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      <AppStack />
    </NavigationContainer>
  )
}

AppNavigator.displayName = "AppNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["splash"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)

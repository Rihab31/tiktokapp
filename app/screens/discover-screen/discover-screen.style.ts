import { StyleSheet, Dimensions } from "react-native"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  logo: {
    height: screenWidth >= 768 ? 250 : 200,
    resizeMode: "contain",
    width: screenWidth >= 768 ? 250 : 200,
  },
  mainContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
})

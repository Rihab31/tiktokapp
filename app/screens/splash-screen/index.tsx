import React, { FC, useEffect } from "react"
import { Image, ImageBackground, View} from "react-native"
import { styles } from "./splash-screen.style"
import { useNavigation } from "@react-navigation/native"
import { COVER, SPLASH } from "../../utils/consts"

const SplashScreen: FC = () => {
  const navigation = useNavigation()

  useEffect(() => {
    setTimeout(() => {
      navigation.reset({
        index: 0,
        routes: [{ name: "home" as never }],
      })
    }, 3000)
  }, [])

  

  return (
    <View style={styles.mainContainer} >
     <Image source={SPLASH} style={styles.logo} resizeMode={COVER}/>
    </View>
  )
}

export default SplashScreen

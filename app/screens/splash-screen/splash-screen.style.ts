import { StyleSheet, Dimensions } from "react-native"
import Colors from "../../utils/colors"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  logo: {
    height:  150,
    resizeMode: "contain",
    width: 150,
  },
  mainContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    backgroundColor:Colors.black
  },
})

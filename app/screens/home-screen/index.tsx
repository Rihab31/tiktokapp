import React, { FC, useRef, useState } from "react"
import {  View, FlatList, Dimensions, Text} from "react-native"
import { styles } from "./home-screen.style"
import Video, { VideoProperties } from 'react-native-video';
import VideoComponent, { VideoComponentProps } from "../../components/video-component";

const HomeScreen: FC = () => {  
const videos = require("../../utils/videos.json")
const [currentIndex,setCurrentIndex] = useState<number>(0)


return (
    <View style={styles.mainContainer}>
       <FlatList
       snapToAlignment="start"
       decelerationRate={"fast"}
       snapToInterval={Dimensions.get("window").height -60}
                  showsVerticalScrollIndicator={false}
                  data={videos.hits}
                
                  onMomentumScrollEnd={(ev) => {
                    const newIndex = Math.round(ev.nativeEvent.contentOffset.y / (Dimensions.get("window").height -60));
                    console.log("Current Index:",newIndex)
                    setCurrentIndex(newIndex)
                  }}
                 
                 
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item,index }) => {
                    const request: VideoComponentProps = {
                      url:item.videos.small.url,
                      userImage:item.userImageURL,
                      currentIndex:currentIndex,
                      itemIndex:index,
                      likes: item.likes,
                      comments:item.comments,
                      userName:item.user,
                      downloads:item.downloads
                    }
                    return (
                      <VideoComponent data={request}/>
                    
                    )
                  }}
                />
     
    </View>
  )
}

export default HomeScreen

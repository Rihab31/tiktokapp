import { StyleSheet, Dimensions } from "react-native"
import Colors from "../../utils/colors"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
 
    backgroundColor:Colors.black
  },
})
